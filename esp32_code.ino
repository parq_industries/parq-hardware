#include <WiFi.h>
#include <Arduino.h>
#include <Wire.h>
#include <PCF8574.h>
#include <PubSubClient.h>
#include <Servo.h>

// Credentials so that ESP32 connects to WLAN
const char* ssid = "---";
const char* password = "---";
const char* mqtt_server = "---";

// Set your Static IP address
IPAddress ip(192, 168, 0, 133);
// Set your Gateway IP address
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 0, 0);

// Initializes the espClient.
WiFiClient esp32Client;
PubSubClient client(esp32Client);

static const int servoPin = 13;
Servo servo1;

// This function reconnects ESP32 to MQTT broker
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP32Client")) {
      Serial.println("connected");  
      // Subscribe or resubscribe to a topic
      client.subscribe("barrier");
      client.subscribe("both_directions");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// This function connects ESP32 to your router
void setup_wifi() {
  delay(1000);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("WiFi connected - ESP IP address: ");
  Serial.println(WiFi.localIP());
}

void setup() {
  // put your setup code here, to run once:
  delay(1000);
  Serial.begin (115200);

  servo1.attach(servoPin);  

  Serial.println("Starting connection");
  WiFi.config(ip, gateway, subnet);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  client.subscribe("barrier");
  client.subscribe("both_directions");

}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i=0;i<length;i++) {
    Serial.print((char)payload[i]);
  }

  if(payload[1] == 'o'){
    for(int posDegrees = 0; posDegrees <= 180; posDegrees++) {
        servo1.write(posDegrees);
        Serial.println(posDegrees);
        delay(20);
    }
  }
  else
  if(payload[1] == 'c'){
    for(int posDegrees = 180; posDegrees >= 0; posDegrees--) {
        servo1.write(posDegrees);
        Serial.println(posDegrees);
        delay(20);
    }
  }
  else return; 
}

void loop()
{
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}